

  <!DOCTYPE html>
  <html lang="es" dir="ltr">
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>encabezado práctica 1</title>





      <!--CDN -->
    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
    <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

  <!-- Optional theme -->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

  <!-- Latest compiled and minified JavaScript -->
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>


  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/provincias.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/contactos.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/estilo.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/index.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/nosotros.css">
    </head>

  <body>

    <!-- Barra de menú-->
        <nav id="barraNavegar" class="navbar navbar-expand-lg navbar-light ">
          <div  class="container-fluid">
          <img class="ima1" src="<?php echo base_url();?>assets/images/ima2.jpg" alt="">  </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item" style="background-color: #74d0e2;border-radius: 25px;">
                  <a class="nav-link active" aria-current="page"  href="<?php echo site_url(); ?>/welcome/index">Inicio</a>
                </li>
                <li class="nav-item" style="background-color: #74d0e2;border-radius: 25px;">
                  <a class="nav-link"href="<?php echo site_url();?>/welcome/nosotros">Nosotros</a>
                </li>
                <li class="nav-item" style="background-color: #74d0e2;border-radius: 25px;">
                  <a class="nav-link" href="<?php echo site_url();?>/welcome/contactos">Contactos</a>
                </li>
                <li class="nav-item" style="background-color: #74d0e2;border-radius: 25px;">
                  <a class="nav-link" href="<?php echo site_url();?>/welcome/provincias">Provincias</a>
                </li>

              </ul>
              <form class="d-flex">
                <input class="form-control me-1" type="search" placeholder="Buscar" aria-label="Search">
                <button class="btn btn-info" type="submit">Buscar</button>
              </form>
            </div>
          </div>
        </nav>
