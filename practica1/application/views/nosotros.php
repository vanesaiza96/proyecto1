
<center><h1><p class="q">Quienes somos</p></h1>
<div class="row">

<div class="col-md-6">
  <center>
  <img class="edi"src="imagenes/nosotros.jpg" alt="Daysi">

</div>
<div class="col-md-6">

  <h4><p class="p">Somos un programa de turismo, en donde ayudamos a la renovación urbana y al desarrllo rural para Incrementar las ofertas de destinos y servicios turisticos sostenible y competitivos en el Ecuador</p></h4>
</div>
</div>
<div class="row">
<div class="col-md-6">
  <h4><p class="pa">Si queremos proteger la biodiversidad, podemos hacerlo. Como consumidores tenemos más poder del que pensamos. Al elegir unos productos sobre otros les decimos a las empresas cómo deben actuar si quieren seguir sus negocios. Si elegimos productos y servicios pensados en clave de sostenibilidad, contribuimos a reducir el impacto en la naturaleza y la biodiversidad, y las empresas apostarán por seguir este camino. </p></h4>
</div>
<div class="col-md-6">
    <center>
    <img class="volcan"src="imagenes/nosotros1.jpg" alt="Daysi">

  </div>
</div>
<iframe width="560" height="315" src="https://www.youtube.com/embed/Aj-Olrcm1Wg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
<hr>
<footer>
  <div class="container">
    <div class="row">
        <div class="col-xs-12 col-md-6">
            <h6 class="text-muted lead">CONTACTO:</h6>
            <h6 class="text-muted">
            Latacunga-Ecuadorr<br>
            San Felipe<br>
            Teléfonos: 0997505065 – 0985956252.<br>
            </h6>
        </div>
        <div class="col-xs-12 col-md-6">
        <div class="pull-right">
        <h6 class="text-muted lead">ENCUENTRANOS EN LAS REDES</h6>
              <div class="redes-footer">
                  <a  href="https://www.facebook.com/"><img  class="redes"src="imagenes/facebook-2.png"></a>
                  <a class="redes" href="https://twitter.com/"><img  class="redes" src="imagenes/wa-2.png"></a>
                  <a  class="redes"href="https://www.youtube.com/"><img  class="redes" src="imagenes/youtube-2.png"></a>
              </div>
        </div>
        <div class="row"> <p class="text-muted small text-right">Realizado por:<br> Vanessa Puco y Daysi Jami.</p></div>
    </div>
  </div>
</div>
</footer>
