
<center> <p style="margin-top:10px;"> <h2>Descubre Esmeraldas</h2></p>
        <h5><p class="pcuenca">Esmeraldas, considerada una de las ciudades más bellas del Ecuador por su arquitectura colonial y republicana, es ideal para el turismo de aventura, cultural, religioso o simplemente para un descanso.</p></h5>
        <div class="row">
          <div class="col-md-12">
            <img   style="  align-items: flex-start; border-radius:25px;"class="imgcuenca1"src="imagenes/esmeraldas.jpg" alt="Daysi">
            <hr>

          </div>
        </div>

        </div>

        <center><h2>¿Que ver y hacer en Esmeraldas?</h2>
          <br><br>
        <h5>
          <p class="p1c">Esmeraldas es conocida, además de por sus hermosas playas, sus paisajes exuberantes y su clima cálido y húmedo, por ser tradicionalmente el territorio afroecuatoriano por excelencia. Así mismo, las selvas esmeraldeñas son cuna de 3 de las 4 nacionalidades indígenas de la Región Litoral de Ecuador: los cayapas , los épera y los awá. El área de la provincia fue cuna de culturas como los Atácame, Tolas, Cayapas. La colonización española se dio el 21 de septiembre de 1526, cuando Bartolomé Ruiz echó anclas en la desembocadura de un «río Grande» y denominó al sector como San Mateo, durante ese período la entidad máxima y precursora de la provincia sería el Gobierno de las Esmeraldas. Después de la guerra independentista y la anexión de Ecuador a la Gran Colombia, se crea la Provincia de Pichincha el 25 de junio de 1824, en la que dentro de sus límites se encuentra el actual territorio esmeraldeño. El 20 de noviembre de 1847 se crea la octava provincia del país, la Provincia de Esmeraldas. Es conocida como la provincia verde por su gran cantidad de productos agrícolas.</p>
            <br>
            <p class="p1c">En el territorio esmeraldeño habitan 491.168 personas, según el último censo nacional (2010), siendo la octava provincia más poblada del país. La Provincia de Esmeraldas está constituida por 7 cantones, con sus respectivas parroquias urbanas y rurales.</p>

            <p class="p1c">Limita al este con Carchi e Imbabura, al sur con Santo Domingo de los Tsáchilas y Manabí, al sureste con Pichincha, al norte con la Provincia de Tumaco-Barbacoas, del departamento de Nariño perteneciente a Colombia, y al oeste y norte con el océano Pacífico a lo largo de una franja marítima de unos 230 kilómetros.</p>
            <br>
            <p class="p1c">Esta situada en la zona geográfica conocida como región litoral o costa. Su capital administrativa es la ciudad de Esmeraldas, la cual además es su urbe más grande y poblada. Ocupa un territorio de unos 14.893 km², siendo la séptima provincia del país por extensión.</p>
          </h5>
          <br>
        <hr>



          <div class="row">
            <div class="col-md-12">
            <p style="margin-top:10px;"> <h3>Gastronomia de Esmeraldas del Ecuador</h3></p>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4">
              <h6><p class="pcm"><b>El Ceviche:</b>  El Ceviche que pueden ser de mariscos, palmito o chochos entre los más comunes, acerca de la comida ecuatoriana,que son tipicas en esmeraldas.<p></h6>
<br>
                <img class="gastronomia" src="imagenes/escomida.jpg" alt="Daysi">
            </div>

            <div class="col-md-4">
              <h6><p class="pcm"><b>El Tapao:</b>
Este es un plato a base de carne de pescado el cual se sazona y se seca al sol, donde luego es cocinada con plátanos y servidas en hojas del mismo.<p></h6>
<br>
                <img class="gastronomia" src="imagenes/escomida.jpg" alt="Daysi">
            </div>

            <div class="col-md-4">
              <h6><p class="pcm"><b>Cangrejo azul:</b> Cangrejo azul
Crustáceo terrestre cocinado en diferentes presentaciones como ceviche, sopa, encocado, ceviche, conchas, camaron apanados etc.<p></h6>
<br>
                <img  class="gastronomia"src="imagenes/escomida.jpg" alt="Daysi">
            </div>

          </div>
<br><br>
