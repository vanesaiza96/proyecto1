<footer>
  <div class="container">
    <div class="row">
        <div class="col-xs-12 col-md-6">
            <h6 class="text-muted lead">CONTACTO:</h6>
            <h6 class="text-muted">
            Latacunga-Ecuadorr<br>
            San Felipe<br>
            Teléfonos: 0997505065 – 0985956252.<br>
            </h6>
        </div>
        <div class="col-xs-12 col-md-6">
        <div class="pull-right">
        <h6 class="text-muted lead">ENCUENTRANOS EN LAS REDES</h6>
              <div class="redes-footer">
                  <a  href="https://www.facebook.com/"><img  class="redes"src="imagenes/facebook-2.png"></a>
                  <a class="redes" href="https://twitter.com/"><img  class="redes" src="imagenes/wa-2.png"></a>
                  <a  class="redes"href="https://www.youtube.com/"><img  class="redes" src="imagenes/youtube-2.png"></a>
              </div>
        </div>
        <div class="row"> <p class="text-muted small text-right">Realizado por:<br> Vanessa Puco y Daysi Jami.</p></div>
    </div>
  </div>
</div>
</footer>


</body>
</html>
