<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index()
	{

		$this->load->view('header');
		$this->load->view('index');
		$this->load->view('footer');
	}
	public function nosotros(){
		$this->load->view('header');
			$this->load->view('nosotros');
		$this->load->view('footer');
	}
	public function contactos(){
		$this->load->view('header');
			$this->load->view('contactos');
		$this->load->view('footer');
	}
	public function provincias(){
		$this->load->view('header');
			$this->load->view('provincias');
		$this->load->view('footer');
	}
}
